#!/bin/bash


########################################################################
#                                                                      #
#         G U E T Z L I   I M A G E   O P T I M I Z A T I O N          #
#                                                                      #
#           SCRIPT TO OPTIMIZE JPG / JPEG IMAGES IN A FOLDER           #
#                                                                      #
#                      Copyright 2017 by PB-Soft                       #
#                                                                      #
#                           www.pb-soft.com                            #
#                                                                      #
#                       Version 1.0 / 08.06.2017                       #
#                                                                      #
########################################################################
#
#  1 - Check that the free 'libpng' software packages is installed on
#      your system (http://www.libpng.org/pub/png/libpng.html).
#
#  2 - Get the 'Guetzli' (Perceptual JPEG Encoder) binary from here:
#      https://github.com/google/guetzli/releases
#
#  3 - Copy the 'Guetzli' binary into the 'guetzli' folder where this
#      script is located.
#
#  4 - Customize the configuration section of this script and specify
#      the paths (if necessary) and the optimization quality.
#
#  5 - Copy some JPG files into the subfolder 'input'.
#
#  6 - Run this script to compress them and get the optimized files
#      from the subfolder 'output'.
#
# ======================================================================
#
#  For more information about the 'Guetzli' application, please visit
#  the following website (GitHub repository):
#
#      https://github.com/google/guetzli 
#
# ======================================================================
#
# Check that the following software packages are installed on your
# system if you want to send emails:
#
#  - sendEmail (lightweight, command line SMTP email client)
#
#  - libnet-ssleay-perl (perl module to call Secure Sockets Layer (SSL)
#                       functions of the SSLeay library)
#
#  - libio-socket-ssl-perl (perl module that uses SSL to encrypt data)
#
# ======================================================================
#
# You can use the following commands to install the packages:
#
#   apt-get install sendemail
#
#   apt-get install libnet-ssleay-perl
#
#   apt-get install libio-socket-ssl-perl
#
# ======================================================================
#
# Check the following link for information about the sendEmail tool:
#
#   http://caspian.dotconf.net/menu/Software/SendEmail/
#
# ======================================================================
#
# Check that the firewall port 587 for SMTP (Simple Mail Transfer Pro-
# tocol) is open if you want to send notification emails!
#
# ======================================================================


########################################################################
########################################################################
##############   C O N F I G U R A T I O N   B E G I N   ###############
########################################################################
########################################################################


# ======================================================================
# Specify the paths.
# ======================================================================
ACTUAL_DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
GUETZLI_PATH=$ACTUAL_DIRECTORY
INPUT_PATH=$GUETZLI_PATH/input
OUTPUT_PATH=$GUETZLI_PATH/output


# ======================================================================
# Specify the default optimization level (quality).
# ======================================================================
DEFAULT_QUALITY=85


# ======================================================================
# Specify the default file extension ('jpg' or 'jpeg').
# ======================================================================
DEFAULT_EXTENSION="jpg"


# ======================================================================
# Specify if all optimized files (output) should have the the default
# file extension ('yes' or 'no').
# ======================================================================
USE_DEFAULT_EXTENSION="yes"


# ======================================================================
# Specify if email notifications should be sent (yes/no).
# ======================================================================
EMAIL_NOTIFICATIONS="yes"


# ======================================================================
# Specify the connection details for the SMTP host.
# ======================================================================
EMAIL_USERNAME="john.doe@gmail.com"
EMAIL_PASSWORD="smtp_password"
EMAIL_HOSTNAME="smtp.gmail.com"
EMAIL_PORT="587"
EMAIL_TLS="yes"


# ======================================================================
# Specify the email sender.
# ======================================================================
EMAIL_SENDER="admin@example.com"


# ======================================================================
# Specify the email receiver(s). Multiple receivers may be specified by
# separating them by either a white space, comma, or semi-colon like:
#
#  EMAIL_RECEIVER="john@example.com brad@domain.com"
#
# ======================================================================
EMAIL_RECEIVER="john.doe@yahoo.com"


########################################################################
########################################################################
################   C O N F I G U R A T I O N   E N D   #################
########################################################################
########################################################################


# ======================================================================
# Clear the screen.
# ======================================================================
clear


# ======================================================================
# Specify the script start time.
# ======================================================================
SCRIPT_START=`date +%s`


# ======================================================================
# Initialize the file counter.
# ======================================================================
FILE_COUNTER=0


# ======================================================================
# Get the paths of the 'sendEmail' executable.
# ======================================================================
SENDEMAIL="$(which sendEmail)"


# ======================================================================
# Get the quality value from the command line.
# ======================================================================
QUALITY=$1


# ======================================================================
# Check if the quality was not set.
# ($# holds the number of arguments passed to the script)
# ======================================================================
if [ $# -eq 0 ]; then


  # ====================================================================
  # Use the default quality value.
  # ====================================================================
  QUALITY=$DEFAULT_QUALITY
fi


########################################################################
########################################################################
###############   C R E A T E   D I R E C T O R I E S   ################
########################################################################
########################################################################


# ======================================================================
# Check if the input directory does not exist.
# ======================================================================
if [ ! -d $INPUT_PATH ]; then


  # ====================================================================
  # Create the input directory.
  # ====================================================================
  mkdir -p $INPUT_PATH
fi


# ======================================================================
# Check if the output directory does not exist.
# ======================================================================
if [ ! -d $OUTPUT_PATH ]; then


  # ====================================================================
  # Create the output directory.
  # ====================================================================
  mkdir -p $OUTPUT_PATH
fi


########################################################################
########################################################################
################   D E L E T E   O L D   I M A G E S   #################
########################################################################
########################################################################


# ======================================================================
# Check if the output directory is not empty.
# ======================================================================
if [ "$(ls -A $OUTPUT_PATH)" ]; then


  # ====================================================================
  # Remove the images in the output directory.
  # ====================================================================
  find $OUTPUT_PATH -maxdepth 1 -type f -iname "*.jpeg" -exec rm {} +
  find $OUTPUT_PATH -maxdepth 1 -type f -iname "*.jpg" -exec rm {} +
fi


########################################################################
########################################################################
###############   I M A G E   O P T I M I Z A T I O N   ################
########################################################################
########################################################################


# ======================================================================
# Display the start bar.
# ======================================================================
echo "================================================================================"
echo
echo "              G U E T Z L I   I M A G E   O P T I M I Z A T I O N"
echo
echo "================================================================================"
echo


# ======================================================================
# Find all images (JPG / JPEG) in the input folder.
# ======================================================================
find $INPUT_PATH -iname "*.jpg" -o -iname "*.jpeg" -o -iname "*.png" |
{


  # ====================================================================
  # Loop through the input folder to process all images (JPG / JPEG).
  # The whole processing operation will run in the subprocess because
  # otherwise the 'Total' variables are empty. Check the following web-
  # site for more information about this behavior:
  #
  #    http://mywiki.wooledge.org/BashFAQ/024
  #
  # ====================================================================
  while read f; do


    # ==================================================================
    # Specify the image optimization start time.
    # ==================================================================
    OPT_START=`date +%s`


    # ==================================================================
    # Increase the file counter.
    # ==================================================================
    FILE_COUNTER=$((FILE_COUNTER + 1))


    # ==================================================================
    # Get the actual filename.
    # ==================================================================
    INPUT_FILENAME=$(basename $f)


    # ==================================================================
    # Get the actual filename without the extension.
    # ==================================================================
    FILENAME_ONLY="${INPUT_FILENAME%.*}"


    # ==================================================================
    # Get the actual file extension.
    # ==================================================================
    EXTENSION="${INPUT_FILENAME##*.}"


    # ==================================================================
    # Check if the file extension is 'png'.
    # ==================================================================
    if [[ "$EXTENSION" == "png" || "$EXTENSION" == "PNG" || "$USE_DEFAULT_EXTENSION" == "yes" ]]; then


      # ================================================================
      # Change the output file extension to 'jpg'.
      # ================================================================
      OUTPUT_FILENAME=$FILENAME_ONLY.$DEFAULT_EXTENSION


      # ================================================================
      # The file extension is not 'png'.
      # ================================================================
    else


      # ================================================================
      # Use the 'normal' extension for the output file.
      # ================================================================
      OUTPUT_FILENAME=$FILENAME_ONLY.$EXTENSION
    fi


    # ==================================================================
    # Display the name of the actual processed image.
    # ==================================================================
    echo "$FILE_COUNTER | Compressing file '$INPUT_FILENAME' with a quality of $QUALITY..."


    # ==================================================================
    # Optimize the actual image.
    # ==================================================================
    $GUETZLI_PATH/guetzli_linux_x86-64 --quality $QUALITY $f $OUTPUT_PATH/$OUTPUT_FILENAME


    # ==================================================================
    # Calculate the size of the actual input file.
    # ==================================================================
    FILESIZE_INPUT=$(stat -c%s "$f")


    # ==================================================================
    # Calculate the total input size.
    # ==================================================================
    TOTAL_FILESIZE_INPUT=$((TOTAL_FILESIZE_INPUT+FILESIZE_INPUT))


    # ==================================================================
    # Calculate the size of the actual output file.
    # ==================================================================
    FILESIZE_OUTPUT=$(stat -c%s "$OUTPUT_PATH/$OUTPUT_FILENAME")


    # ==================================================================
    # Calculate the total output size.
    # ==================================================================
    TOTAL_FILESIZE_OUTPUT=$((TOTAL_FILESIZE_OUTPUT+FILESIZE_OUTPUT))


    # ==================================================================
    # Specify the image optimization end time.
    # ==================================================================
    OPT_END=`date +%s`


    # ==================================================================
    # Calculate the image optimization processing time.
    # ==================================================================
    OPT_TIME=$((OPT_END-OPT_START))


    # ==================================================================
    # Check if the actual image could not be optimized.
    # ==================================================================
    if [ "$FILESIZE_OUTPUT" -ge "$FILESIZE_INPUT" ]; then


      # ================================================================
      # Display an information message.
      # ================================================================
      echo "The image could not be optimized! (tried for $OPT_TIME seconds)"


      # ================================================================
      # Check if the actual file extension is 'png'.
      # ================================================================
      if [[ "$EXTENSION" == "png" || "$EXTENSION" == "PNG" ]]; then


        # ==============================================================
        # Display an information message.
        # ==============================================================
        echo "The original file was NOT copied to the output directory because it's a *.$EXTENSION file!"
        echo


        # ==============================================================
        # The actual file extension is not 'png'.
        # ==============================================================
      else


        # ==============================================================
        # Copy the original file to the output directory.
        # ==============================================================
        cp $f $OUTPUT_PATH/$OUTPUT_FILENAME


        # ==============================================================
        # Display an information message.
        # ==============================================================
        echo "The original file was copied to the output directory..."
        echo
      fi


      # ================================================================
      # The image could be optimized.
      # ================================================================
    else


      # ================================================================
      # Calculate the size difference of the actual file - Percent.
      # ================================================================
      DIFFERENCE_PERCENT=$(bc <<< "scale=2; ($FILESIZE_INPUT-$FILESIZE_OUTPUT)*10000/$FILESIZE_INPUT/-100")


      # ================================================================
      # Calculate the size difference of the actual file - KB.
      # ================================================================
      DIFFERENCE_KB=$(bc <<< "scale=2; ($FILESIZE_INPUT-$FILESIZE_OUTPUT)/-1024")


      # ================================================================
      # Display the size difference of the actual file.
      # ================================================================
      echo "Size difference of file $FILE_COUNTER: $DIFFERENCE_PERCENT% or ${DIFFERENCE_KB}KB (optimized in $OPT_TIME seconds)"
      echo
    fi
  done


  ######################################################################
  ######################################################################
  ###################   S H O W   O V E R V I E W   ####################
  ######################################################################
  ######################################################################

 
  # ====================================================================
  # Calculate the size difference of the actual file - Percent.
  # ====================================================================
  TOTAL_DIFFERENCE_PERCENT=$(bc <<< "scale=2; ($TOTAL_FILESIZE_INPUT-$TOTAL_FILESIZE_OUTPUT)*10000/$TOTAL_FILESIZE_INPUT/-100")


  # ====================================================================
  # Calculate the size difference of the actual file - KB.
  # ====================================================================
  TOTAL_DIFFERENCE_KB=$(bc <<< "scale=2; ($TOTAL_FILESIZE_INPUT-$TOTAL_FILESIZE_OUTPUT)/-1024")


  # ====================================================================
  # Display the size difference of the actual file.
  # ====================================================================
  echo "================================================================================"
  echo
  echo "Total size difference: $TOTAL_DIFFERENCE_PERCENT% or ${TOTAL_DIFFERENCE_KB}KB"
  echo


  # ====================================================================
  # Specify the script end time.
  # ====================================================================
  SCRIPT_END=`date +%s`


  # ====================================================================
  # Calculate the script runtime.
  # ====================================================================
  SCRIPT_TIME=$((SCRIPT_END-SCRIPT_START))


  # ====================================================================
  # Display the script runtime.
  # ====================================================================
  echo "Total processing time: $SCRIPT_TIME seconds"
  echo
  echo "================================================================================"
  echo


  ######################################################################
  ######################################################################
  #############   E - M A I L   N O T I F I C A T I O N   ##############
  ######################################################################
  ######################################################################


  # ====================================================================
  # Check if email notifications should be sent.
  # ====================================================================
  if [ "$EMAIL_NOTIFICATIONS" == "yes" ]; then


    # ==================================================================
    # Specify the actual date in the 'dd.mm.yyyy' format.
    # ==================================================================
    EMAIL_DATE="$(date +"%d.%m.%Y")"


    # ==================================================================
    # Specify the actual time in the 'hh:mm:ss' format.
    # ==================================================================
    EMAIL_TIME="$(date +"%H:%M:%S")"


    # ==================================================================
    # Get the actual hostname where the script is running.
    # ==================================================================
    SCRIPT_HOST="$(hostname)"


    # ==================================================================
    # Specify more email details.
    # ==================================================================
    EMAIL_SUBJECT="$EMAIL_DATE / $EMAIL_TIME | Guetzli optimization of $FILE_COUNTER files terminated!"
    EMAIL_MESSAGE="Start date: $EMAIL_DATE\nStart time: $EMAIL_TIME\nHostname: $SCRIPT_HOST\nNumber of files: $FILE_COUNTER\nProcessing time: $SCRIPT_TIME s\nSize difference in %: $TOTAL_DIFFERENCE_PERCENT\nSize difference in KB: $TOTAL_DIFFERENCE_KB"


    # ==================================================================
    # Send a notification via email.
    # ==================================================================
    $SENDEMAIL -o tls=$EMAIL_TLS -f $EMAIL_SENDER -t $EMAIL_RECEIVER -s $EMAIL_HOSTNAME:$EMAIL_PORT -xu $EMAIL_USERNAME -xp $EMAIL_PASSWORD -u "$EMAIL_SUBJECT" -m "$EMAIL_MESSAGE" -o message-charset=utf-8


    # ==================================================================
    # Display the end line.
    # ==================================================================
    echo
    echo "================================================================================"
    echo
  fi
}
