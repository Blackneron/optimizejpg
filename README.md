# OptimizeJPG Script - README #
---

### Overview ###

The **OptimizeJPG** script (Bash) uses the [**Guetzli Perceptual JPEG encoder**](https://github.com/google/guetzli) to optimize all **JPG** and **PNG** files in a specified folder and sends you an email notification if the process has finished. The script will also display information about the optimization progress and the size difference of each image file due to the compression. The script can be customized in the configuration section.


### Setup ###

* Install the software packages **sendemail**, **libnet-ssleay-perl** and **libio-socket-ssl-perl**.
* You can use the command: **apt-get install sendemail libnet-ssleay-perl libio-socket-ssl-perl**
* Copy the script folder **optimizejpg** to your computer.
* Make the optimizer script **optimizejpg.sh** executable: **chmod +x optimizejpg.sh**.
* Edit the configuration section of the **optimizejpg.sh** script to fit your needs.
* Open the firewall port **587** if you want to send notification emails!
* Copy some images (JPG or PNG) into the **input** subfolder.
* Start the optimizer script from the command prompt: **./optimizejpg.sh**
* Optional you can specify a quality level as a parameter: **./optimizejpg.sh 90**
* Get the optimized images from the **output** subfolder and check if a notification was sent by email.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### Licenses ###

The **OptimizeJPG** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT). The [**Guetzli Perceptual JPEG encoder**](https://github.com/google/guetzli) is licensed under the [**Apache License (Version 2.0)**](https://github.com/google/guetzli/blob/master/LICENSE). More information about this license is available on the [**Apache Website**](https://www.apache.org/licenses/).
